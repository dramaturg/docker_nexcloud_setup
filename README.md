installls nextcloud, postgres, nginx as seperate container


# requirements

- [docker compose v2](https://docs.docker.com/compose/migrate/)
- docker 

# before you can start
## systemd service file
copy the service file to: `/etc/systemd/system/nextcloud-docker.service`

```
systemctl daemon-reload
systemclt enable --now nextcloud-docker.service
```

## create a config.php (or copy the example
the config.php file needs to be in the config folder

## create the password and user secret files
```
./postgres_db.txt
./postgres_password.txt
./postgres_user.txt
```


# set a new admin password
```
docker exec -ti --user www-data nextcloud-app-1 /var/www/html/occ user:resetpassword admin
Enter a new password:
Confirm the new password:
Successfully reset password for admin
```

# index all files into nextcloud
```
docker exec -ti --user www-data nextcloud-app-1 /var/www/html/occ files:scan --all
```
